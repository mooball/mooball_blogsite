# -*- coding: utf-8 -*-
from mooball.blog.content.blog import IBlog  # NOQA E501
from mooball.blog.testing import MOOBALL_BLOG_INTEGRATION_TESTING  # noqa
from plone import api
from plone.app.testing import setRoles
from plone.app.testing import TEST_USER_ID
from plone.dexterity.interfaces import IDexterityFTI
from zope.component import createObject
from zope.component import queryUtility

import unittest


try:
    from plone.dexterity.schema import portalTypeToSchemaName
except ImportError:
    # Plone < 5
    from plone.dexterity.utils import portalTypeToSchemaName


class BlogFolderIntegrationTest(unittest.TestCase):

    layer = MOOBALL_BLOG_INTEGRATION_TESTING

    def setUp(self):
        """Custom shared utility setup for tests."""
        self.portal = self.layer['portal']
        setRoles(self.portal, TEST_USER_ID, ['Manager'])

    def test_ct_blog_schema(self):
        fti = queryUtility(IDexterityFTI, name='Blog')
        schema = fti.lookupSchema()
        self.assertEqual(IBlog, schema)

    def test_ct_blog_fti(self):
        fti = queryUtility(IDexterityFTI, name='Blog')
        self.assertTrue(fti)

    def test_ct_blog_factory(self):
        fti = queryUtility(IDexterityFTI, name='Blog')
        factory = fti.factory
        obj = createObject(factory)

        self.assertTrue(
            IBlog.providedBy(obj),
            u'IBlog not provided by {0}!'.format(
                obj,
            ),
        )

    def test_ct_blog_adding(self):
        setRoles(self.portal, TEST_USER_ID, ['Contributor'])
        obj = api.content.create(
            container=self.portal,
            type='Blog',
            id='blog',
        )

        self.assertTrue(
            IBlog.providedBy(obj),
            u'IBlog not provided by {0}!'.format(
                obj.id,
            ),
        )
