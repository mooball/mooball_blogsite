# -*- coding: utf-8 -*-
from mooball.blog.content.post import IPost  # NOQA E501
from mooball.blog.testing import MOOBALL_BLOG_INTEGRATION_TESTING  # noqa
from plone import api
from plone.app.testing import setRoles
from plone.app.testing import TEST_USER_ID
from plone.dexterity.interfaces import IDexterityFTI
from zope.component import createObject
from zope.component import queryUtility

import unittest


try:
    from plone.dexterity.schema import portalTypeToSchemaName
except ImportError:
    # Plone < 5
    from plone.dexterity.utils import portalTypeToSchemaName


class BlogViewIntegrationTest(unittest.TestCase):

    layer = MOOBALL_BLOG_INTEGRATION_TESTING

    def setUp(self):
        """Custom shared utility setup for tests."""
        self.portal = self.layer['portal']
        setRoles(self.portal, TEST_USER_ID, ['Manager'])
        portal_types = self.portal.portal_types
        parent_id = portal_types.constructContent(
            'Blog',
            self.portal,
            'parent_id',
            title='Parent container',
        )
        self.parent = self.portal[parent_id]

    def test_ct_post_schema(self):
        fti = queryUtility(IDexterityFTI, name='Post')
        schema = fti.lookupSchema()
        self.assertEqual(IPost, schema)

    def test_ct_post_fti(self):
        fti = queryUtility(IDexterityFTI, name='Post')
        self.assertTrue(fti)

    def test_ct_post_factory(self):
        fti = queryUtility(IDexterityFTI, name='Post')
        factory = fti.factory
        obj = createObject(factory)

        self.assertTrue(
            IPost.providedBy(obj),
            u'IPost not provided by {0}!'.format(
                obj,
            ),
        )

    def test_ct_post_adding(self):
        setRoles(self.portal, TEST_USER_ID, ['Contributor'])
        obj = api.content.create(
            container=self.parent,
            type='Post',
            id='post',
        )

        self.assertTrue(
            IPost.providedBy(obj),
            u'IPost not provided by {0}!'.format(
                obj.id,
            ),
        )
