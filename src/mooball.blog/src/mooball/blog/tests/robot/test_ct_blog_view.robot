# ============================================================================
# DEXTERITY ROBOT TESTS
# ============================================================================
#
# Run this robot test stand-alone:
#
#  $ bin/test -s mooball.blog -t test_blog_view.robot --all
#
# Run this robot test with robot server (which is faster):
#
# 1) Start robot server:
#
# $ bin/robot-server --reload-path src mooball.blog.testing.MOOBALL_BLOG_ACCEPTANCE_TESTING
#
# 2) Run robot tests:
#
# $ bin/robot /src/mooball/blog/tests/robot/test_blog_view.robot
#
# See the http://docs.plone.org for further details (search for robot
# framework).
#
# ============================================================================

*** Settings *****************************************************************

Resource  plone/app/robotframework/selenium.robot
Resource  plone/app/robotframework/keywords.robot

Library  Remote  ${PLONE_URL}/RobotRemote

Test Setup  Open test browser
Test Teardown  Close all browsers


*** Test Cases ***************************************************************

Scenario: As a site administrator I can add a Blog View
  Given a logged-in site administrator
    and an add Blog_Folder form
   When I type 'My Blog View' into the title field
    and I submit the form
   Then a Blog View with the title 'My Blog View' has been created

Scenario: As a site administrator I can view a Blog View
  Given a logged-in site administrator
    and a Blog View 'My Blog View'
   When I go to the Blog View view
   Then I can see the Blog View title 'My Blog View'


*** Keywords *****************************************************************

# --- Given ------------------------------------------------------------------

a logged-in site administrator
  Enable autologin as  Site Administrator

an add Blog_Folder form
  Go To  ${PLONE_URL}/++add++Blog_Folder

a Blog View 'My Blog View'
  Create content  type=Blog_Folder  id=my-blog_view  title=My Blog View

# --- WHEN -------------------------------------------------------------------

I type '${title}' into the title field
  Input Text  name=form.widgets.IBasic.title  ${title}

I submit the form
  Click Button  Save

I go to the Blog View view
  Go To  ${PLONE_URL}/my-blog_view
  Wait until page contains  Site Map


# --- THEN -------------------------------------------------------------------

a Blog View with the title '${title}' has been created
  Wait until page contains  Site Map
  Page should contain  ${title}
  Page should contain  Item created

I can see the Blog View title '${title}'
  Wait until page contains  Site Map
  Page should contain  ${title}
