# -*- coding: utf-8 -*-
"""Setup tests for this package."""
from plone import api
from plone.app.testing import setRoles
from plone.app.testing import TEST_USER_ID
from mooball.blog.testing import MOOBALL_BLOG_INTEGRATION_TESTING  # noqa

import unittest


class TestSetup(unittest.TestCase):
    """Test that mooball.blog is properly installed."""

    layer = MOOBALL_BLOG_INTEGRATION_TESTING

    def setUp(self):
        """Custom shared utility setup for tests."""
        self.portal = self.layer['portal']
        self.installer = api.portal.get_tool('portal_quickinstaller')

    def test_product_installed(self):
        """Test if mooball.blog is installed."""
        self.assertTrue(self.installer.isProductInstalled(
            'mooball.blog'))

    def test_browserlayer(self):
        """Test that IMooballBlogLayer is registered."""
        from mooball.blog.interfaces import (
            IMooballBlogLayer)
        from plone.browserlayer import utils
        self.assertIn(
            IMooballBlogLayer,
            utils.registered_layers())


class TestUninstall(unittest.TestCase):

    layer = MOOBALL_BLOG_INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']
        self.installer = api.portal.get_tool('portal_quickinstaller')
        roles_before = api.user.get_roles(TEST_USER_ID)
        setRoles(self.portal, TEST_USER_ID, ['Manager'])
        self.installer.uninstallProducts(['mooball.blog'])
        setRoles(self.portal, TEST_USER_ID, roles_before)

    def test_product_uninstalled(self):
        """Test if mooball.blog is cleanly uninstalled."""
        self.assertFalse(self.installer.isProductInstalled(
            'mooball.blog'))

    def test_browserlayer_removed(self):
        """Test that IMooballBlogLayer is removed."""
        from mooball.blog.interfaces import \
            IMooballBlogLayer
        from plone.browserlayer import utils
        self.assertNotIn(
            IMooballBlogLayer,
            utils.registered_layers())
