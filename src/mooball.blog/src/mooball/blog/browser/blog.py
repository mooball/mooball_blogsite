# -*- coding: utf-8 -*-

from Products.Five import BrowserView
from plone import api
# from zope.component import getUtility


class BlogView(BrowserView):

    def __init__(self, context, request):
        self.context = context
        self.request = request

    def get_results(self):
        catalog = api.portal.get_tool('portal_catalog')
        text = self.request.form.get(u'text', None)

        query = {'portal_type': 'Blog'}

        if text:
            query['SearchableText'] = text

        result = catalog(query)
        return result
