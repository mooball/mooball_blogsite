# -*- coding: utf-8 -*-
from plone.app.robotframework.testing import REMOTE_LIBRARY_BUNDLE_FIXTURE
from plone.app.testing import applyProfile
from plone.app.testing import FunctionalTesting
from plone.app.testing import IntegrationTesting
from plone.app.testing import PLONE_FIXTURE
from plone.app.testing import PloneSandboxLayer
from plone.testing import z2

import mooball.blog


class MooballBlogLayer(PloneSandboxLayer):

    defaultBases = (PLONE_FIXTURE,)

    def setUpZope(self, app, configurationContext):
        # Load any other ZCML that is required for your tests.
        # The z3c.autoinclude feature is disabled in the Plone fixture base
        # layer.
        import plone.app.dexterity
        self.loadZCML(package=plone.app.dexterity)
        self.loadZCML(package=mooball.blog)

    def setUpPloneSite(self, portal):
        applyProfile(portal, 'mooball.blog:default')


MOOBALL_BLOG_FIXTURE = MooballBlogLayer()


MOOBALL_BLOG_INTEGRATION_TESTING = IntegrationTesting(
    bases=(MOOBALL_BLOG_FIXTURE,),
    name='MooballBlogLayer:IntegrationTesting',
)


MOOBALL_BLOG_FUNCTIONAL_TESTING = FunctionalTesting(
    bases=(MOOBALL_BLOG_FIXTURE,),
    name='MooballBlogLayer:FunctionalTesting',
)


MOOBALL_BLOG_ACCEPTANCE_TESTING = FunctionalTesting(
    bases=(
        MOOBALL_BLOG_FIXTURE,
        REMOTE_LIBRARY_BUNDLE_FIXTURE,
        z2.ZSERVER_FIXTURE,
    ),
    name='MooballBlogLayer:AcceptanceTesting',
)
