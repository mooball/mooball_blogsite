# -*- coding: utf-8 -*-
from plone.app.textfield import RichText
from plone.autoform import directives
from plone.dexterity.content import Container
from plone.namedfile import field as namedfile
from plone.supermodel import model
from plone.supermodel.directives import fieldset
from z3c.form.browser.radio import RadioFieldWidget
from zope import schema
from zope.interface import implementer
from mooball.blog import _


class IBlog(model.Schema):
    """ Marker interface and Dexterity Python Schema for Blog
    """

    # directives.widget(level=RadioFieldWidget)
    # level = schema.Choice(
    #     title=_(u'Sponsoring Level'),
    #     vocabulary=LevelVocabulary,
    #     required=True
    # )
    # title = schema.ASCII(
    #     title=_(u'Title'),
    #     min_length=15,
    #     max_length=30
    # )

    # description = schema.Text(
    #     title=_(u'Description'),
    #     required=False,
    # )

    body = RichText(
        title=_(u'Body'),
        required=False
    )

    categories_vocabulary = schema.List(
        title=_(u'Categories'),
        description=_(u'Blog Categories'),
        min_length=1,
        value_type=schema.TextLine(
            title=_(u'Text')
        )
    )

    full_post = schema.Bool(
        title=_(u'Display Full Posts'),
        default=False,
        required=False
    )

    category_navigation = schema.Bool(
        title=_(u'Display Category Navigation'),
        default=False,
        required=False
    )

    archive_navigation = schema.Bool(
        title=_(u'Display Archive Navigation'),
        default=False,
        required=False
    )

    post_per_page = schema.Int(
        title=_(u'Post per Page'),
        default=5,
        min=0,
        max=10,
        required=False
    )

    # url = schema.URI(
    #     title=_(u'Link'),
    #     required=False
    # )

    # fieldset('Images', fields=['logo', 'advertisement'])
    # logo = namedfile.NamedBlobImage(
    #     title=_(u'Logo'),
    #     required=False,
    # )

    # advertisement = namedfile.NamedBlobImage(
    #     title=_(u'Advertisement (Gold-sponsors and above)'),
    #     required=False,
    # )

    # directives.read_permission(notes='cmf.ManagePortal')
    # directives.write_permission(notes='cmf.ManagePortal')
    # notes = RichText(
    #     title=_(u'Secret Notes (only for site-admins)'),
    #     required=False
    # )


@implementer(IBlog)
class Blog(Container):

    # def __init__(self, request, context):
    #     self.context = context
    #     self.request = request

    def blog_content(self):
        return 'Hello'
