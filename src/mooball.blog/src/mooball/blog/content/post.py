# -*- coding: utf-8 -*-
from plone.app.textfield import RichText
from plone.autoform import directives
from plone.dexterity.content import Item
from plone.namedfile import field as namedfile
from plone.supermodel import model
from plone.supermodel.directives import fieldset
from z3c.form.browser.radio import RadioFieldWidget
from zope import schema
from zope.interface import implementer
from mooball.blog import _


class IPost(model.Schema):
    """ Marker interface and Dexterity Python Schema for Post
    """

    # directives.widget(level=RadioFieldWidget)
    # level = schema.Choice(
    #     title=_(u'Sponsoring Level'),
    #     vocabulary=LevelVocabulary,
    #     required=True
    # )

    # title = schema.ASCII(
    #     title=_(u'Title'),
    #     min_length=15,
    #     max_length=30
    # )

    # description = schema.Text(
    #     title=_(u'Description'),
    #     required=False,
    # )

    body = RichText(
        title=_(u'Body'),
        required=False
    )

    categories = schema.Choice(
        title=_(u'Select Category'),
        vocabulary='mooball.blog.BlogCategories',
        required=False
    )

    pinned = schema.Bool(
        title=_(u'Pinned'),
        default=False,
        required=False
    )

    # url = schema.URI(
    #     title=_(u'Link'),
    #     required=False
    # )

    # fieldset('Images', fields=['logo', 'advertisement'])
    # logo = namedfile.NamedBlobImage(
    #     title=_(u'Logo'),
    #     required=False,
    # )

    # advertisement = namedfile.NamedBlobImage(
    #     title=_(u'Advertisement (Gold-sponsors and above)'),
    #     required=False,
    # )

    # directives.read_permission(notes='cmf.ManagePortal')
    # directives.write_permission(notes='cmf.ManagePortal')
    # notes = RichText(
    #     title=_(u'Secret Notes (only for site-admins)'),
    #     required=False
    # )


@implementer(IPost)
class Post(Item):

    def blog_content(self):
        return 'Hello'
